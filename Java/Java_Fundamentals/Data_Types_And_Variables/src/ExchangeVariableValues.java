import java.util.*;
public class ExchangeVariableValues {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        
        int temp = a;  // a ^= b;
        a = b;         // b ^= a;
        b = temp;      // a ^= b;
        
        System.out.println(a + "\n" + b);
    }
}
