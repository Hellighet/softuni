/**
 * The problem was supposed to be solved in C#.
 * As there isn't an unsigned byte in Java, I just imitated it.
 */

import java.util.*;
public class IncrementVariable {
    public static void main(String args[]){
        Scanner x = new Scanner(System.in);
        
        int n = x.nextInt();
        int count = 0;
        short limit = 0;
        for (int i = 0; i < n; i++) {
            if(limit == 256){
                count++;
                limit = 0;
            }
            limit++;
        }
        
        System.out.println(limit);
        if(count != 0){
            System.out.printf("Overflowed %d times",count);
        }
    }
}
