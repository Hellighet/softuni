public class PracticeIntegerNumbers {
    public static void main(String args[]){
        
        byte bm = -100;
        short bp = 128;
        short sm = -3540;
        int uShort = 64876;
        long uInt = 2147483648L;
        long sInt = -1141583228L;
        long l = -1223372036854775808L;
       
        System.out.println(bm);
        System.out.println(bp);
        System.out.println(sm);
        System.out.println(uShort);
        System.out.println(uInt);
        System.out.println(sInt);
        System.out.println(l);
    }
}
