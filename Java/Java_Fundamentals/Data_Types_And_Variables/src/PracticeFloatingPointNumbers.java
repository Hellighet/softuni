import java.math.BigDecimal;

public class PracticeFloatingPointNumbers {
    public static void main(String args[]){
        BigDecimal x = new BigDecimal("3.141592653589793238");
        double d = 1.60217657d;
        BigDecimal y = new BigDecimal("7.8184261974584555216535342341");
        
        System.out.println(x + "\n" + d + "\n" + y); 
    }
}
