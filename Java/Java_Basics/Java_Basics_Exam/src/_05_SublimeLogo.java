
import java.util.*;

public class _05_SublimeLogo {

    public static String newString(char ch, int n) {
        if (n > 0) {
            return new String(new char[n]).replace('\0', ch);
        } else {
            return "";
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int stars = 2;

        //head
        for (int i = 0; i < n; i++) {
            System.out.print(newString(' ', n * 2 - stars));
            System.out.print(newString('*', stars));
                stars += 2;
            System.out.println();
        }
        
        //body
        stars -= 4;
        for (int i = 0; i < 3; i++) {
            System.out.print(newString('*', stars));
            System.out.print(newString(' ', n * 2 - stars));
            if (i < 1) {
               stars -=2;
            }else{
                stars +=2;
            }
            System.out.println();
        }
            System.out.print(newString('*', stars) + "\n");
            stars-=2;
        for (int i = 0; i < 3; i++) {
            System.out.print(newString(' ', n * 2 - stars));
            System.out.print(newString('*', stars));
            if (i < 1) {
               stars -=2;
            }else{
                stars +=2;
            }
            System.out.println();
        }
        
            //footer
            stars = n * 2;
            for (int i = 0; i < n; i++) {
                System.out.print(newString('*', stars));
                System.out.print(newString(' ', n * 2 - stars));
                stars -= 2;
                System.out.println();
            }

        }
    }
