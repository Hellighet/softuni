import java.util.*;
public class _06_SumAndProduct {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        int prod = 1;
        int sum = 0;
        
        
        for (int a = 1; a <= 9; a++) {
            for (int b = 9; b >= 1; b--) {
                for (int c = 0; c < 9; c++) {
                    for (int d = 9; d >= 0; d--) {
                        prod = a*b*c*d;
                        sum = a + b + c + d;
                        if(prod / sum == 3 && n%3==0)
                        {
                            System.out.printf("%d%d%d%d",d,c,b,a);
                            return;
                        }
                        else if(prod == sum && n % 10 == 5){
                            System.out.printf("%d%d%d%d",a,b,c,d);
                            return;
                        }
                    }
                }
            }
            sum = 0;
            prod = 1;
        }
        System.out.println("Nothing found");
    }
}
