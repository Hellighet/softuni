
import java.util.*;

public class _04_CreditSystem {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int grade = 0;
        double sumGrade = 0;
        int credit = 0;
        double total = 0;

        for (int i = 0; i < n; i++) {
            int num = input.nextInt();
            grade = num % 10;
            num = num / 10;
            sumGrade += grade;
            credit = num % 100;

            switch (grade) {
                case 3:
                    total += (credit * 0.5);
                    break;
                case 4:
                    total += (credit * 0.7);
                    break;
                case 5:
                    total += (credit * 0.85);
                    break;
                case 6:
                    total += credit;
                    break;
                default:
                    break;
            }
        }

        System.out.printf("%.2f%n", total);
        System.out.printf("%.2f", sumGrade / n);
    }
}
