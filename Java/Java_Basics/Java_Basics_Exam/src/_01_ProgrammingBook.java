import java.util.*;

public class _01_ProgrammingBook {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        double priceOnePage = input.nextDouble();
        double priceOneCover = input.nextDouble();
        int discountPaper = input.nextInt();
        double sumDesigner = input.nextDouble();
        int totalPercent = input.nextInt();
        double money;
        
        money = priceOnePage * 899 + priceOneCover*2;
        money = money*(1-(discountPaper/100.0));
        money = money + sumDesigner;
        money = money *(1-(totalPercent/100.0));
        
        System.out.printf("Avtonom should pay %.2f BGN.", money);
        
    }
}
