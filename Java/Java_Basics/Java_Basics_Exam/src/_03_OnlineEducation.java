
import java.util.*;

public class _03_OnlineEducation {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String type;
        int count;
        int onsite = 0;
        int online = 0;
        int total = 0;

        for (int i = 0; i < 3; i++) {
            type = input.nextLine();
            count = Integer.parseInt(input.nextLine());
            if (type.equals("onsite")) {
                if (onsite < 600) {
                    if (count > 600) {
                        onsite += 600;
                        online += count - 600;
                    } else {
                        onsite += count;
                        if(onsite>600){
                            online += onsite - 600;
                            onsite -= onsite - 600;
                        }
                    }
                } else {
                    online += count;
                }
            } else {
                online += count;
            }

            total += count;
        }

        System.out.printf("Online students: %d%n", online);
        System.out.printf("Onsite students: %d%n", onsite);
        System.out.printf("Total students: %d%n", total);
    }
}
