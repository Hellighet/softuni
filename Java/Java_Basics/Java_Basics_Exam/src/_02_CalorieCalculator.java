import java.util.*;

public class _02_CalorieCalculator {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        String gender = input.nextLine();
        double kg = Double.parseDouble(input.nextLine());
        double height = Double.parseDouble(input.nextLine());
        int age = Integer.parseInt(input.nextLine());
        String activity = input.nextLine();
        double calories;
        
        double BNM;
        
        if(gender.equals("m")){
            BNM = 66 + (13.7 * kg) + (5.0*height*100) - (6.8 * age);
        }else{
            BNM = 655 + (9.6 * kg) + (1.8*height*100) - (4.7 * age);
        }
       
        switch(activity){
            case "sedentary":
                calories = BNM * 1.2;
                break;
            case "lightly active":
                calories = BNM * 1.375;
                break;
            case "moderately active":
                calories = BNM * 1.55;
                break;
            default:
                calories = BNM * 1.725;
                break;
        }
        
        
        
        System.out.printf("To maintain your current weight you will need %d calories per day.",(int)Math.ceil(calories));
    }
}