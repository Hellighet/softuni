
import java.util.*;

public class _06_MinNumber {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int min = Integer.MAX_VALUE;
        int n = input.nextInt();

        for (int i = 0; i < n; ++i) {
            int num = input.nextInt();
            if (min > num) {
                min = num;
            }
        }
        input.close();

        System.out.println(min);
    }
}
