
import java.util.*;

public class _08_OddEvenSum {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int oddSum = 0, evenSum = 0;
        int n = input.nextInt();

        for (int i = 0; i < n; ++i) {
            int num = input.nextInt();
            if(i % 2 == 0){
                evenSum += num;
            }else{
                oddSum +=num;
            }
        }
        
        if(evenSum == oddSum){
            System.out.printf("Yes%nSum = %d",evenSum);
        }else{
            System.out.printf("No%nDiff = %d",Math.abs(evenSum - oddSum));
        }
    }
}
