import java.util.*;
public class _12_EqualPairs {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        int[] arr = new int[n];
        int maxDiff = 0;
        int diff = 0;
        
        for(int i =0;i<n;i++){
            int num1 = input.nextInt();
            int num2 = input.nextInt();
            arr[i] = num1 + num2;
            if(i!=0){
                if(arr[i] != arr[i-1]){
                    diff = Math.abs(arr[i] - arr[i-1]);
                    if(maxDiff < diff){
                        maxDiff = diff;
                    }
                }
            }
        }
        
        if(maxDiff == 0){
            System.out.printf("Yes,value=%d",arr[0]);
        }else{
            System.out.printf("No, maxdiff=%d",maxDiff);
        }
    }
}
