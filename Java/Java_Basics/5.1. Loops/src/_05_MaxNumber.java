import java.util.*;

public class _05_MaxNumber {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int max = Integer.MIN_VALUE;
        int n = input.nextInt();
        
        for(int i=0;i<n;++i){
            int num = input.nextInt();
            if(max < num){
                max = num;
            }
        }
        System.out.println(max);
    }
}
