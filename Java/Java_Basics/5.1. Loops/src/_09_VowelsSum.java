import java.util.*;
public class _09_VowelsSum {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int sum = 0;
        String text = input.nextLine().toLowerCase();
        
        for(int i = 0; i < text.length();i++){
            if(text.charAt(i) == 'a'){
                sum += 1;
            }else if(text.charAt(i) == 'e'){
                sum+=2;
            }else if(text.charAt(i) == 'i'){
                sum+=3;
            }else if(text.charAt(i) == 'o'){
                sum+=4;
            }else if(text.charAt(i) == 'u'){
                sum+=5;
            }
        }
        
        System.out.println(sum);
    }
    
}
