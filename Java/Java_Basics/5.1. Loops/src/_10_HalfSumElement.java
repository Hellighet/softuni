import java.util.*;
public class _10_HalfSumElement {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        int sum = 0;
        int max = Integer.MIN_VALUE;
        
        
        for(int i =0;i < n;++i){
            int num = input.nextInt();
            sum += num;
            if(max < num){
                max = num;
            }
        }
        
        if(max == (sum-max)){
            System.out.printf("Yes%nSum = %d", max);
        }else{
            System.out.printf("No%nDiff = %d",Math.abs(max-(sum-max)));
        }
    }
}
