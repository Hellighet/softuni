
import java.util.*;

public class _07_LeftAndRightSum {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int sumLeft = 0;
        int sumRight = 0;

        for (int i = 0; i < n * 2; ++i) {
            int num = input.nextInt();
            if (i < n) {
                sumLeft += num;
            } else {
                sumRight +=num;
            }
        }
        
        if(sumLeft == sumRight){
            System.out.printf("Yes, sum = %d%n",sumLeft);
        }else{
            System.out.printf("No, diff = %d%n",Math.abs(sumLeft - sumRight));
        }
    }
}
