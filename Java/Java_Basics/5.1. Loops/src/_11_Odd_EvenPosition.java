
import java.util.*;
import java.text.*;

public class _11_Odd_EvenPosition {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        DecimalFormat format = new DecimalFormat("#.#####");
        
        int n = input.nextInt();

        double evenSum = 0.0;
        double evenMax = -1000000.0;
        double evenMin = Double.MAX_VALUE;

        double oddSum = 0.0;
        double oddMax = -1000000.0;
        double oddMin = Double.MAX_VALUE;

        for (int i = 1; i <= n; ++i) {
            double num = input.nextDouble();
            if (i % 2 == 0) {
                evenSum += num;
                if (evenMax < num) {
                    evenMax = num;
                }
                if (evenMin > num) {
                    evenMin = num;
                }
            } else {
                oddSum += num;
                System.out.println("OddMax: " + oddMax +">" + num);
                if (oddMax < num) {
                    oddMax = num;
                }
                System.out.println("OddMin: " + oddMin +">" + num);
                if (oddMin > num) {
                    
                    oddMin = num;
                }
            }
        }
        if (oddMax != -1000000.0 && oddMin < Double.MAX_VALUE) {
            System.out.printf("OddSum=%s,%nOddMin=%s,%nOddMax=%s,",format.format(oddSum),format.format(oddMin),format.format(oddMax));
        }else{
            System.out.printf("OddSum=%s,%nOddMin=No,%nOddMax=No,",format.format(oddSum));
        }
        if (evenMax != -1000000.0 && evenMin < Double.MAX_VALUE) {
            System.out.printf("%nEvenSum=%s,%nEvenMin=%s,%nEvenMax=%s",format.format(evenSum),format.format(evenMin),format.format(evenMax));
        }else{
            System.out.printf("%nEvenSum=%s,%nEvenMin=No,%nEvenMax=No",format.format(evenSum));
        }
    }
}
