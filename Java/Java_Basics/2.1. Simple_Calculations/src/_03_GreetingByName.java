import java.util.*;

public class _03_GreetingByName {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		String name = input.nextLine();
		input.close();
		
		System.out.printf("Hello, %s!%n",name);
	}
}
