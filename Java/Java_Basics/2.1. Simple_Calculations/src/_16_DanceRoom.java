import java.util.*;

public class _16_DanceRoom {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double length = input.nextDouble();
		double width = input.nextDouble();
		double wardrobe = input.nextDouble();
		
		double AreaCM = (length*100)*(width*100);
		double wardrobeAreaCM = ((wardrobe*100)*(wardrobe*100));
		double benchArea = AreaCM /10;
		double freeSpace = AreaCM - wardrobeAreaCM - benchArea;
		double dancers = freeSpace /(40 + 7000);
		
		System.out.println((int)dancers);
	}
}
