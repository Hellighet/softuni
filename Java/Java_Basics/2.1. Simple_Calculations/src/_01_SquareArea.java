import java.util.*;

public class _01_SquareArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		double a = input.nextDouble();
		input.close();

		System.out.println(a * a);

	}

}
