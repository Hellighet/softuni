import java.util.*;

public class _02_InchesToCentimeters {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		double inch = input.nextDouble();
		input.close();
		double centimeter = inch * 2.54;

		System.out.println(centimeter);

	}

}
