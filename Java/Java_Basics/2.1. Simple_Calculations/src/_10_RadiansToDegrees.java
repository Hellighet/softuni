import java.util.*;

public class _10_RadiansToDegrees {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double radians = input.nextDouble();
		input.close();
		
		double degrees = radians*(180/Math.PI);
		
		System.out.println(Math.round(degrees));
	}
}
