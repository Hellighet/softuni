import java.util.*;

public class _09_CelsiusToFahrenheit {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double celsius = input.nextDouble();
		input.close();
		
		double fahrenheit = celsius*1.8 + 32;
		
		System.out.printf("%.2f",fahrenheit);
	}

}
