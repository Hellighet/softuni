import java.util.*;

public class _12_CurrencyConverter {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double sum = input.nextDouble();
		input.nextLine();
		String curr = input.nextLine();
		String currResult = input.nextLine();
		
		double USD = 1.79549;
		double EUR = 1.95583;
		double GBP = 2.53405;
		
		switch(curr) {
		case "USD":
			sum = sum*USD;
			break;
		case "EUR":
			sum = sum*EUR;
			break;
		case "GBP":
			sum = sum*GBP;
			break;
		}
		
		switch(currResult) {
		case "USD":
			sum = sum/USD;
			break;
		case "EUR":
			sum = sum/EUR;
			break;
		case "GBP":
			sum = sum/GBP;
			break;
		}
		
		System.out.printf("%.2f %s", sum, currResult);
		
	}
}
