import java.util.Scanner;

public class _14_Aquarium {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		double length = input.nextDouble();
		double width = input.nextDouble();
		double height = input.nextDouble();
		double percentage = input.nextDouble();
		input.close();
		
		double liters = (length*width*height)*0.001;
		percentage = percentage*0.01;
		
		double result = liters*(1-percentage);
		
		System.out.printf("%.3f",result);
		
		

	}

}
