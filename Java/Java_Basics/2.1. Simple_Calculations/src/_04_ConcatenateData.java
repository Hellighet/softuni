import java.util.*;

public class _04_ConcatenateData {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String firstName = input.nextLine();
		String lastName = input.nextLine();
		int age = input.nextInt();
		input.nextLine();
		String town = input.nextLine();
		input.close();
		
		System.out.printf("You are %s %s, a %d-years old person from %s.",firstName,lastName,age,town);
	}

}
