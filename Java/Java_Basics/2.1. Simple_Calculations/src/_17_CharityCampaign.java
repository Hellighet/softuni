import java.util.*;

public class _17_CharityCampaign {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int days = input.nextInt();
		int pastryChefs = input.nextInt();
		int cakes = input.nextInt();
		int waffles = input.nextInt();
		int pancakes = input.nextInt();
		input.close();
		
		double cakesPrice = cakes*45;
		double wafflePrice = waffles*5.80;
		double pancakePrice = pancakes*3.20;
		double totalSum = (cakesPrice + wafflePrice + pancakePrice)*pastryChefs;
		double totalSumCollected = totalSum * days;
		double totalExpenses = totalSumCollected - totalSumCollected/8;
		
		System.out.printf("%.2f",totalExpenses);
		

	}

}
