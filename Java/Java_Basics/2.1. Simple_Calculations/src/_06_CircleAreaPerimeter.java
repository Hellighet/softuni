import java.util.*;

public class _06_CircleAreaPerimeter {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double r = input.nextDouble();
		input.close();
		
		System.out.println("Area = " + r*r*Math.PI);
		System.out.println("Area = " + 2*Math.PI*r);
	}
}
