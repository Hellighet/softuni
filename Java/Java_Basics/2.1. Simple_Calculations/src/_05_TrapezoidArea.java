import java.util.*;
public class _05_TrapezoidArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double b1 = input.nextDouble();
		double b2 = input.nextDouble();
		double h = input.nextDouble();
		input.close();
		
		System.out.println((b1+b2)*h/2.0);

	}

}
