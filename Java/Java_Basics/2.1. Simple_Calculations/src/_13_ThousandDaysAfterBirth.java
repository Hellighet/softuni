import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class _13_ThousandDaysAfterBirth {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate date = LocalDate.parse(input.nextLine(),formatter);
		input.close();
		LocalDate newDate = date.plusDays(1000);
		
		System.out.printf("%s",newDate.format(formatter));

	}

}
