import java.util.*;

public class _15_TailorFactory {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		int tables = input.nextInt();
		double tableLength = input.nextDouble();
		double tableWidth = input.nextDouble();
		input.close();
		double AreaTop = tables*((tableLength + 2*0.30)*(tableWidth + 2*0.30));
		double AreaTop2 = tables*((tableLength/2)*(tableLength/2));
		
		double priceUSD = AreaTop * 7 + AreaTop2*9;
		double priceBGN = priceUSD * 1.85;
		
		System.out.printf("%.2f %s%n%.2f %s",priceUSD,"USD",priceBGN,"BGN");
		
	}
}
