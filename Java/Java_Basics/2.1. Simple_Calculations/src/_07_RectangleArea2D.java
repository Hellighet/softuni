import java.text.DecimalFormat;
import java.util.*;

public class _07_RectangleArea2D {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#.#####");
		
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		input.close();
		
		double width = Math.abs(x1 - x2);
		double height = Math.abs(y1 - y2);
		
		System.out.println(df.format(width*height));
		System.out.println(df.format((2*(width+height))));
		
	}
}
