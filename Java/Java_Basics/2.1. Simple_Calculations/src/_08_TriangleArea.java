import java.util.*;
public class _08_TriangleArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double a = input.nextDouble();
		double h = input.nextDouble();
		input.close();
		
		double result = a*(h/2.0); 
		
		System.out.printf("Triangle area = %.2f",result);

	}

}
