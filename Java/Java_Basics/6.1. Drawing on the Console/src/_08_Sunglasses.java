
import java.util.*;

public class _08_Sunglasses {

    public static String newString(char ch, int size) {
        return new String(new char[size]).replace('\0', ch);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();

        //header
        System.out.print(newString('*', n * 2));
        System.out.print(newString(' ', n));
        System.out.print(newString('*', n * 2));
        System.out.println();
        //body
        for (int i = 0; i < n - 2; i++) {
            System.out.print("*");
            System.out.print(newString('/', n * 2 - 2));
            System.out.print("*");
            if (i == (n - 1) / 2 - 1) {
                System.out.print(newString('|',n));
            } else {
                System.out.print(newString(' ', n));
            }
            System.out.print("*");
            System.out.print(newString('/', n * 2 - 2));
            System.out.print("*");
            System.out.println();
        }
        //footer
        System.out.print(newString('*', n * 2));
        System.out.print(newString(' ', n));
        System.out.print(newString('*', n * 2));

    }
}
