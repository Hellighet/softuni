
import java.util.*;

public class _05_SquareFrame {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();

        //head
        System.out.print("+");
        for (int i = 0; i < n - 2; i++) {
            System.out.print(" -");
        }
        System.out.print(" +");
        System.out.println();

        //body
        for (int i = 0; i < n-2; i++) {
            System.out.print("|");
            
            for (int j = 0; j < n - 2; j++) {
                System.out.print(" -");
            }
            System.out.print(" |");
            System.out.println();
        }
        
        //footer
        System.out.print("+");
        for (int i = 0; i < n - 2; i++) {
            System.out.print(" -");
        }
        System.out.print(" +");


    }
}
