import java.util.*;

public class _09_House {
    
    public static String newString(char ch, int size){
        return new String(new char[size]).replace('\0',ch);
    }
    public  static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        int stars;
        if(n%2 == 0){
            stars = 2;
        }else{
            stars = 1;
        }
        
        for (int i = 0; i < (n+1)/2; i++) {
                System.out.print(newString('-',(n-stars)/2));
                System.out.print(newString('*',stars));
                System.out.print(newString('-',(n-stars)/2));
                System.out.println();
                stars +=2;
        }
        for (int i = 0; i < n/2; i++) {
            System.out.print("|");
            System.out.print(newString('*',n-2));
            System.out.print("|");
            System.out.println();
        }
    }
}
