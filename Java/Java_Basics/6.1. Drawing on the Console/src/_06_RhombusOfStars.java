
import java.util.*;

public class _06_RhombusOfStars {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        
        for (int row = 1; row <= n; row++) {
            for (int j = 0; j < n-row; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < row; k++) {
                System.out.print("* ");    
            }
            System.out.println();
        }
        
        for (int row = n-1; row >= 1; row--) {
            for (int j = 0; j < n-row; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < row; k++) {
                System.out.print("* ");    
            }
            System.out.println();
        }
    }
}
