
import java.util.*;

public class _10_Diamond {

    public static String newString(char ch, int size) {
        return new String(new char[size]).replace('\0', ch);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        input.close();
        int leftRight = (n - 1) / 2; //5 - 1 / 2 = 2 -- -- = 0
        int midPoint;
        int rows;
        if (n % 2 == 0) {
            midPoint = (n - 1) / 2;
            rows = n - 1;
        } else {
            midPoint = n / 2;
            rows = n;
        }

        if (n == 1) {
            System.out.println(newString('*', n));
        } else if (n == 2) {
            System.out.println(newString('*', n));
        } else {

            for (int i = 0; i < rows; i++) {
                int mid = n - 2 * leftRight - 2;

                if (leftRight > 0) {
                    System.out.print(newString('-', leftRight));
                }
                System.out.print("*");
                if (mid >= 0) {
                    System.out.print(newString('-', mid));
                    System.out.print("*");
                }
                if (leftRight > 0) {
                    System.out.print(newString('-', leftRight));
                }
                if (i < midPoint) {
                    leftRight--;
                } else {
                    leftRight++;
                }
                System.out.println();
            }
        }
    }
}
