
import java.util.*;

public class _07_ChristmasTree {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();

        for (int i = 1; i <= n + 1; i++) {
            for (int j = 0; j <= n-i; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < i-1; k++) {
                System.out.print("*");
            }
            System.out.print(" | ");
            
            for (int x = 0; x < i-1; x++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
