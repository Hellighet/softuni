import java.util.*;
public class _10_AnimalType {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        String animal = input.nextLine();
        input.close();
        
        switch(animal){
            case "crocodile":
            case "tortoise":
            case "snake":
                System.out.println("reptile");
                break;
            case "dog":
                System.out.println("mammal");
            default:
                System.out.println("unknown");
        }
        
    }
}
