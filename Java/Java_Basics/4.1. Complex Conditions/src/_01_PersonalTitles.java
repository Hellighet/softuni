import java.util.*;

public class _01_PersonalTitles {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double age = input.nextDouble();
        input.nextLine();
        String gender = input.nextLine();
        input.close();

        if (age < 16 && gender.equals("m")) {
            System.out.println("Master");
        } else if (age > 15 && gender.equals("m")) {
            System.out.println("Mr.");
        } else if (age < 16 && gender.equals("f")) {
            System.out.println("Miss");
        } else {
            System.out.println("Ms.");
        }

    }

}