
import java.util.*;

public class _04_FruitOrVegetable {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String object = input.nextLine();
        
        switch(object){
            case "apple":
            case "kiwi":
            case "cherry":
            case "lemon":
            case "grapes":
            case "banana":
                System.out.println("fruit");
                break;
            case "tomato":
            case "cucumber":
            case "pepper":
            case "carrot":
                System.out.println("vegetable");
                break;
            default: 
                System.out.println("unknown");
        }
    }
}
