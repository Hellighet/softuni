import java.util.*;

public class _05_InvalidNumber {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int number = input.nextInt();
        
        if(!((number >= 100 && number<=200) || (number == 0)))
            System.out.println("invalid");
    }
}
