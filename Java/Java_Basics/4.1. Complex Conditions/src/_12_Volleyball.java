
import java.util.*;

public class _12_Volleyball {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String year = input.nextLine();
        int p = Integer.parseInt(input.nextLine());
        int h = Integer.parseInt(input.nextLine());

        int weekends = 48 - h;
        double games = weekends * (3.0 / 4.0);

        games += h;
        games += p * (2.0 / 3.0);

        if (year.equals("leap")) {
            games += games * 0.15;
        }
        System.out.println((int)games);
    }
}
