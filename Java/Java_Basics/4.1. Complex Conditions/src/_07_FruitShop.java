
import java.util.*;

public class _07_FruitShop {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String fruit = input.nextLine();
        String day = input.nextLine();
        double quantity = Double.parseDouble(input.nextLine());
        input.close();
        double result = -1.0;

        switch (fruit) {
            case "banana":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 2.5;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 2.7;
                }
                break;
            case "apple":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 1.2;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 1.25;
                }
                break;
            case "orange":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 0.85;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 0.9;
                }
                break;
            case "grapefruit":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 1.45;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 1.6;
                }
                break;
            case "kiwi":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 2.7;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 3.0;
                }
                break;
            case "pineapple":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 5.5;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 5.6;
                }
                break;
            case "grapes":
                if (day.matches("Monday|Tuesday|Wednesday|Thursday|Friday")) {
                    result = quantity * 3.85;
                } else if (day.matches("Saturday|Sunday")) {
                    result = quantity * 4.2;
                }
                break;
        }
        if (result >= 0) {
            System.out.println(result);
        } else {
            System.out.println("error");
        }
    }
}
