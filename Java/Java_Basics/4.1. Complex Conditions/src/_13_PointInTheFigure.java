
import java.util.*;

public class _13_PointInTheFigure {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double h = Double.parseDouble(input.nextLine());
        double x = Double.parseDouble(input.nextLine());
        double y = Double.parseDouble(input.nextLine());
        input.close();

        if (((y > 0 && y < h) && (x > 0 && x < 3 * h))
                || ((y > 0 && y < 4 * h) && (x > h && x < 2 * h))) {
            System.out.println("inside");
        } else if (((x < h && y > h) || (x > 2 * h && y > h)) || (x > 3 * h) || (y < 0) || (x < 0) || (y > 4 * h)) {
            System.out.println("outside");
        } else {
            System.out.println("border");
        }
    }
}
