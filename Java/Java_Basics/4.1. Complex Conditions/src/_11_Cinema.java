import java.util.*;

public class _11_Cinema {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        String movieType = input.nextLine();
        int rows = Integer.parseInt(input.nextLine());
        int cols = Integer.parseInt(input.nextLine());
        double total = -1;
        
        
        switch(movieType){
            case "Premiere":
                total = (rows*cols)*12.00;
                break;
            case "Normal":
                total = (rows*cols)*7.50;
                break;
            case "Discount":
                total = (rows*cols)*5.00;
                break;
        }
        
        System.out.printf("%.2f leva",total);
    }
}
