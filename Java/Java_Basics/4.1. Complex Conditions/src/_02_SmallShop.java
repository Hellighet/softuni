import java.util.*;
public class _02_SmallShop {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        String product = input.nextLine();
        String city = input.nextLine();
        double quantity = input.nextDouble();
        double price = 0;
        
        switch(city){
            case "Sofia":
                if(product.equals("coffee")){
                    price = quantity*0.5;
                }else if(product.equals("peanuts")){
                    price = quantity*1.6;
                }else if(product.equals("beer")){
                    price = quantity*1.2;
                }else if(product.equals("water")){
                    price = quantity*0.8;
                }else{
                    price = quantity*1.45;
                }
                break;
            case "Plovdiv":
                if(product.equals("coffee")){
                    price = quantity*0.4;
                }else if(product.equals("peanuts")){
                    price = quantity*1.5;
                }else if(product.equals("beer")){
                    price = quantity*1.15;
                }else if(product.equals("water")){
                    price = quantity*0.7;
                }else{
                    price = quantity*1.3;
                }
                break;
            case "Varna":
                if(product.equals("coffee")){
                    price = quantity*0.45;
                }else if(product.equals("peanuts")){
                    price = quantity*1.55;
                }else if(product.equals("beer")){
                    price = quantity*1.1;
                }else if(product.equals("water")){
                    price = quantity*0.7;
                }else{
                    price = quantity*1.35;
                }
                break;
        }
        System.out.println(price);
    }
}
