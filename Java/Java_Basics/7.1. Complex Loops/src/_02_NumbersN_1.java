
import java.util.*;

public class _02_NumbersN_1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        
        for (int i = n; i > 0; --i) {
            System.out.println(i);
        }
    }
}
