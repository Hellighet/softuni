
import java.util.*;

public class _14_NumberTable {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int num;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                num = i + j + 1;
                if (num > n) {
                    num = 2 * n - num;
                }
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
