
import java.util.*;

public class _05_Sequence2kPlus1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        
        for (int i = 1; i <= n; i=i*2+1) {
            System.out.println(i);
        }
    }
}
