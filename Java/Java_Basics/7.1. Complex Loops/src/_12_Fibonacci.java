import java.util.*;

public class _12_Fibonacci {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        
        // 1 1 2 3 4 = 
        
        if(n < 2){
            System.out.println(1);
        }else{
            int prev = 1;
            int fibonacci = 1;
            for (int i = 1; i < n; i++) {
                fibonacci += prev;
                prev = fibonacci - prev;
            }
            
            System.out.println(fibonacci);
        }
    }
}
