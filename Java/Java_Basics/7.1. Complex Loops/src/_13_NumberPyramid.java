import java.util.*;

public class _13_NumberPyramid {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();;
        int cnt = 0;
        int delimeter = 1;
        
        for (int i = 1; i <= n; i++) {
            System.out.print(i + " ");
            cnt++;
            if(cnt == delimeter){
                System.out.println();
                delimeter++;
                cnt = 0;
            }
            
        }
    }
}
