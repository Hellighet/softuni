
import java.util.*;

public class _06_NumberInRange1_100 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        
        do{
            System.out.println("Еnter a number in the range [1...100]:");
            n = input.nextInt();
            
            if(n < 1 || n > 100){
                System.out.println("Invalid number!");
            }
        }while(n < 1 || n > 100);
        
        System.out.printf("The number is: %d",n);
    }
}
