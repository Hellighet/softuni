
import java.util.*;

public class _03_PowersOfTwo {

    public static int power(int a, int b) {
        int temp = 1;

        for (int i = 0; i < b; i++) {
            temp *=a;
        }
        return temp;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i <= n; i++) {
            System.out.println(power(2,i));
        }
    }
}
