import java.util.*;

public class _10_CheckPrime {
    
    public static boolean IsPrime(int number){
        boolean isPrime = number > 1;
        
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if(number%i == 0){
                isPrime = false;
            }
        }
        
        return isPrime;
    }
    
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();
        
        if(IsPrime(n)){
            System.out.println("Prime");
        }else{
            System.out.println("Not Prime");
        }
    }
}
