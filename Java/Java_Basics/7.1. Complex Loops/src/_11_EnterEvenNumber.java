
import java.util.*;

public class _11_EnterEvenNumber {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = 1;

        do {
            try {
                n = input.nextInt();
                if (n % 2 == 1) {
                    System.out.println("The number is not even.");
                }
            } catch (Exception e) {
                System.out.println("Invalid number!");
                input.nextLine();
            }
        } while (n % 2 != 0);

        System.out.printf("Even number entered: %d%n", n);
    }
}
