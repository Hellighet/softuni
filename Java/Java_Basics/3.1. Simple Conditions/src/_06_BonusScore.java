import java.text.DecimalFormat;
import java.util.*;

public class _06_BonusScore {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		DecimalFormat format = new DecimalFormat("#.#####");
		
		int score = input.nextInt();
		input.close();
		double bonus = 0;

		if (score <= 100) {
			bonus = 5;
		} else if (score > 100 && score < 1000) {
			bonus = score * 0.2;
		} else if(score > 1000){
			bonus = score * 0.1;
		}
		
		if(score%2 == 0) {
			bonus +=1;
		} else if(score%10 == 5) {
			bonus += 2;
		}
		
		System.out.println(format.format(bonus));
		System.out.println(format.format(score + bonus));

	}

}
