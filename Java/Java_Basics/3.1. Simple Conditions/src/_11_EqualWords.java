import java.util.*;

public class _11_EqualWords {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		String word1 = input.nextLine().toLowerCase();
		String word2 = input.nextLine().toLowerCase();
		input.close();

		if (word1.equals(word2)) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}

	}

}
