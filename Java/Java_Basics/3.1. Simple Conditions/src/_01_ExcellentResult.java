import java.util.*;

public class _01_ExcellentResult{
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double grade = input.nextDouble();
		input.close();
		
		if(grade >= 5.50) {
			System.out.println("Excellent!");
		}
		
	}
}