import java.util.*;

public class _04_GreaterNumber {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		
		double x = input.nextDouble();
		double y = input.nextDouble();
		input.close();
		
		if(x > y) {
			System.out.println(x);
		}else {
			System.out.println(y);
		}
		
	}
}
