import java.util.*;

public class _07_SumSeconds {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);

		int secondsFirst = input.nextInt();
		int secondsSecond = input.nextInt();
		int secondsThird = input.nextInt();
		input.close();

		int totalSeconds = secondsFirst + secondsSecond + secondsThird;
		int minutes = totalSeconds / 60;
		totalSeconds %= 60;

		if (totalSeconds <= 9)
			System.out.printf("%d:0%d", minutes, totalSeconds);
		else
			System.out.printf("%d:%d", minutes, totalSeconds);
	}
}
