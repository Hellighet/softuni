import java.util.*;

public class _16_NumberToText {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		 int number = input.nextInt();
		 input.close();

		String[] str1 = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven",
				"twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eigteen", "nineteen" };

		String[] str2 = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
			if (number < 0 || number > 100) {
				System.out.println("invalid number");
			} else if (number == 0) {
				System.out.println("zero");
			} else if (number == 100) {
				System.out.println("one hundred");
			} else {
				if (number < 20) {
					System.out.println(str1[number - 1]);
				} else {
					if (number % 10 > 0) {
						System.out.println(str2[number / 10 - 2] + " " + str1[number % 10 - 1]);
					} else {
						System.out.println(str2[number / 10 - 2]);
					}
				}
			}
	}
}