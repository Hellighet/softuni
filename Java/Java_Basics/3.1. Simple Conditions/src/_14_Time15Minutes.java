import java.util.*;

public class _14_Time15Minutes {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int hours = input.nextInt();
		int minutes = input.nextInt();
		input.close();

		minutes += 15;

		if (minutes > 59) {
			hours++;
			minutes -= 60;
			if (hours > 23) {
				hours -= 24;
			}
		}
		
		if (minutes > 9)
			System.out.printf("%d:%d%n", hours, minutes);
		else
			System.out.printf("%d:0%d%n", hours, minutes);

	}

}
