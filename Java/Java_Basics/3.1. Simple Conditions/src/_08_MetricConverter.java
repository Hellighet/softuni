import java.math.BigDecimal;
import java.util.*;

//Use BigDecimal next time

public class _08_MetricConverter {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);

		double number = input.nextDouble();
		input.nextLine();
		String from = input.nextLine();
		String to = input.nextLine();
		input.close();

		switch (from) {
		case "m": number = number*1.0;break;
		case "mm":
			number = number / 1000.0;
			break;
		case "cm":
			number = number / 100.0;
			break;
		case "mi":
			number = number / 0.000621371192;
			break;
		case "in":
			number = number / 39.3700787;
			break;
		case "km":
			number = number * 1000.0;
			break;
		case "ft":
			number = number / 3.2808399;
			break;
		case "yd":
			number = number / 1.093611;
			break;
		default:
			break;
		}

		switch (to) {
		case "m": number = number*1.0;break;
		case "mm":
			number = number * 1000.0;
			break;
		case "cm":
			number = number * 100.0;
			break;
		case "mi":
			number = number * 0.000621371192;
			break;
		case "in":
			number = number * 39.3700787;
			break;
		case "km":
			number = number * 0.001;
			break;
		case "ft":
			number = number * 3.2808399;
			break;
		case "yd":
			number = number * 1.093611;
			break;
		default:
			break;
		}

		System.out.printf("%.8f", number);
	}
}
