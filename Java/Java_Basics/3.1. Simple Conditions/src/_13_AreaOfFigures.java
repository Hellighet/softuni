import java.util.*;
import java.text.*;

public class _13_AreaOfFigures {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		DecimalFormat format = new DecimalFormat("#.###");
		double result = 0;
		
		String figure = input.nextLine();
		
		switch(figure) {
		case "square":
			double aSquare = input.nextDouble();
			result = aSquare*aSquare;
			break;
		case "triangle":
			double aTriangle = input.nextDouble();
			double height = input.nextDouble();
			result = aTriangle*(height/2);
			break;
		case "circle":
			double radius = input.nextDouble();
			result = radius*radius*Math.PI;
			break;
		case "rectangle":
			double aRect = input.nextDouble();
			double bRect = input.nextDouble();
			result = aRect*bRect;
			break;
		}

		System.out.println(format.format(result));
	}

}
