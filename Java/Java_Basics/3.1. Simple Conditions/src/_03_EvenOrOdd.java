import java.util.*;

public class _03_EvenOrOdd {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int number = input.nextInt();
		input.close();
		
		if(number%2==0) {
			System.out.println("even");
		}else {
			System.out.println("odd");
		}

	}

}
